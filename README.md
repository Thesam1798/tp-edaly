## TP Edaly

Tp  écrit par [Aurelient](https://github.com/aurelient) et accessible sur ce lien le 10/01/19 : [https://aurelient.github.io/mif13/2018/TP2/](https://aurelient.github.io/mif13/2018/TP2/)

----

## Build Setup

``` bash
# installer les dépendances
npm install

# Servir avec recharge à chaud sur localhost:8080
npm run dev

# construction pour la production avec minification
npm run build

# construire pour la production et visualiser le rapport de l'analyseur de bundle
npm run build --report

# effectuer des tests unitaires
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

Pour des explications détaillées sur le fonctionnement, consultez [http://vuejs.github.io/vue-loader](http://vuejs.github.io/vue-loader)
