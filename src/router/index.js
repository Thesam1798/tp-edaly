import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/vue/HelloWorld';
import AroundMe from '@/vue/Moi';
import MyAddresses from '@/vue/Addresse';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'HelloWorld',
            component: HelloWorld,
        }, {
            path: '/aroundme',
            name: 'Moi',
            component: AroundMe,
        }, {
            path: '/myaddresses',
            name: 'Adresse',
            component: MyAddresses,
        },
    ],
});
